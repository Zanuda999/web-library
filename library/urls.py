from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.views.generic import TemplateView, RedirectView

from .views import *

urlpatterns = patterns('',
    url(r'^$', LoginView.as_view(), name='library_home'),
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout_then_login',
        kwargs={'login_url': reverse_lazy('login')}, name='logout'),
    url(r'^registration/$', RegisterView.as_view(), name='registration'),
    url(r'^search/$', SearchView.as_view(), name='library_search'),
    url(r'^add-book/$', AddBookView.as_view(), name='add_book'),
    url(r'^success/$', TemplateView.as_view(template_name='library/success.html'), name='success'),
    url(r'^book/$', BookListView.as_view(), name='book_list'),
    url(r'^book/create/$', BookCreateView.as_view(), name='book_create'),
    url(r'^book/(?P<pk>\d+)/update/$', BookUpdateView.as_view(), name='book_update'),
    url(r'^book/(?P<pk>\d+)/delete/$', BookDeleteView.as_view(), name='book_deete'),

    url(r'^year/$', YearListView.as_view(), name='year_list'),
    url(r'^changes/$', ChangedBookListView.as_view(), name='changed_book_list'),
    url(r'^new/$', NewBookListView.as_view(), name='new_book_list'),

    url(r'^author/$', AuthorListView.as_view(), name='author_list'),

    url(r'^book/(?P<pk>\d+)/$', BookDetailView.as_view(), name='book_detail'),
    url(r'^year/(?P<year>\d{4})/$', YearDetailView.as_view(), name='year_detail'),

    url(r'^author/(?P<pk>\d+)/$', AuthorDetailView.as_view(), name='author_detail'),
   )

