from django.forms import ModelForm
from library.models import Book


# Create the form class.
class AddBookForm(ModelForm):

    class Meta:
        model = Book
        fields = [
            'title',
            'year_published',
            'authors',
            'comment',
            'genre',
            'seria',
            'file'
        ]
