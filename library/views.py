import operator
import json
from django.contrib.auth.decorators import login_required
from django.core.serializers.json import DjangoJSONEncoder
from django.core import serializers
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect, HttpResponse
from django.utils.decorators import method_decorator
from django.views.generic import FormView, View
from django.contrib.auth.views import AuthenticationForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
from django.views.generic import ListView, DetailView
from django.views.generic import YearArchiveView
from django.views.generic import UpdateView, CreateView, DeleteView
from library.forms import AddBookForm
from library.models import Seria, Genre
from django.db.models import Q

from .models import Book, Author


class RegisterView(FormView):
    template_name = "library/registration.html"
    form_class = UserCreationForm

    def form_valid(self, form):
        form.save()
        user = authenticate(
            username=form.cleaned_data['username'],
            password=form.cleaned_data['password1'])
        login(self.request, user)
        return HttpResponseRedirect(reverse_lazy('book_list'))


class AddBookView(FormView):
    template_name = "library/add_book.html"
    form_class = AddBookForm

    def form_valid(self, form):
        if self.request.user.is_authenticated():
            form.save()
            return HttpResponseRedirect(reverse_lazy('success'))
        else:
            return HttpResponseRedirect(reverse_lazy('login'))


class LoginView(FormView):
    template_name = "library/login.html"
    form_class = AuthenticationForm

    def form_valid(self, form):
        user = authenticate(
            username=form.cleaned_data['username'],
            password=form.cleaned_data['password'])
        login(self.request, user)
        return HttpResponseRedirect(reverse_lazy('book_list'))


class SearchView(ListView):
    http_method_names = ('get',)
    template_name = 'library/search_results.html'
    model = Book

    def get_queryset(self):
        query = self.request.GET.get('q', u'').split()
        self.query = u' '.join(query)
        qs = self.model._default_manager
        if not query:
            return qs.none()
        q_objects = []
        for term in query:
            q_objects.append(Q(title__icontains=term))
            q_objects.append(Q(sortkey__icontains=term))


        q_objects = reduce(operator.or_, q_objects)
        return qs.select_related('location').filter(q_objects)

    def render_to_response(self, context, **response_kwargs):
        context['query'] = self.query
        return self.response_class(
                request = self.request,
                template = self.get_template_names(),
                context = context,
                **response_kwargs
        )

class BookListView(ListView):
    model = Book

    @method_decorator(login_required(login_url=reverse_lazy('login')))
    def dispatch(self, *args, **kwargs):
        return super(BookListView, self).dispatch(*args, **kwargs)


class YearListView(ListView):
    queryset = Book.objects.order_by('year_published')

class ChangedBookListView(ListView):
    queryset = Book.objects.order_by('-modified')[:10]
    template_name = 'library/book_list_with_dates.html'

class NewBookListView(ListView):
    queryset = Book.objects.order_by('-created')[:10]
    template_name = 'library/book_list_with_dates.html'

class YearDetailView(YearArchiveView):
    queryset = Book.objects.all()
    allow_empty = True 
    allow_future = True
    date_field = 'year_published'
    make_object_list = True

class BookDetailView(DetailView):
    model = Book

class BookUpdateView(UpdateView):
    model = Book

class BookDeleteView(DeleteView):
    model = Book
    success_url = reverse_lazy('book_list')

class BookCreateView(CreateView):
    model = Book

class AuthorListView(ListView):
    model = Author

class AuthorDetailView(DetailView):
    model = Author



class AutoCompleteView(View):
    def get(self, *args, **kwargs):
        books = []
        for i in serializers.serialize('python', Book.objects.filter(title__startswith=self.request.GET.get('term', '')), fields=("title", )):
            books.append(i['fields']['title'])
        return HttpResponse(json.dumps(books))