��          �      \      �     �     �  3   �               &     -     2     8     =     F     K     X     a     h     n     v     |  B   �  �  �     �     �  M   �     �                1     :     C     R     m     r     w  
   �  
   �     �  
   �  (   �  j   �                                  
                                       	                          Add Add book Are you sure you want to delete "%(object.title)s"? Author name Created Delete File Genre Home Modified Nope Original key Register Search Seria Sign in Title Your book added successfully Your search for "%(query)s" returned %(object_list.count)s results Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-04-03 16:16+0300
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Додати Додати книгу Ви впевнені, що хочете видалити "%(object.title)s"? Ім'я автора Створено Видалити Файл Жанр Головна Відредаговано Ні ISBN Зареєструватись Пошук Серія Ввійти Назва Книжка успішно додана По Вашому запиту "%(query)s" було знайдено %(object_list.count)s записів. 