from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.views.generic import TemplateView
from library.views import *
from library.models import *

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from tastypie.resources import ModelResource
class BookResource(ModelResource):
    class Meta:
        queryset = Book.objects.all()
        resource_name = 'books'

entry_resource = BookResource()



urlpatterns = patterns('',
    (r'^api/', include(entry_resource.urls)),
    url(r'^autocomplete/$', AutoCompleteView.as_view()),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^$', include('library.urls')),

    # Examples:
    # url(r'^$', 'library.views.home', name='home'),
    url(r'^', include('library.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)